resource "aws_sns_topic" "cloudtrail" {
  name = "cloudtrail-cis-benchmark"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-unauth-denied-access" {
  name           = "errorCode-AccessDenied-errorCode-UnauthorizedOperation"
  pattern        = "{ ($.errorCode = \"*UnauthorizedOperation\") || ($.errorCode = \"AccessDenied*\") }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "Unauthorized Or Denied Access"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-unauth-denied-access" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-unauth-denied-access"
  alarm_description   = "Alarms if cloudtrail logs show unauthroized or denied access attempts."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Unauthorized Or Denied Access"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-network-gateway-change" {
  name           = "network-gateway-change"
  pattern        = "{ ($.eventName = CreateCustomerGateway) || ($.eventName = DeleteCustomerGateway) || ($.eventName = AttachInternetGateway) || ($.eventName = CreateInternetGateway) || ($.eventName = DeleteInternetGateway) || ($.eventName = DetachInternetGateway) }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "Network Gateway Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-network-gateway-change" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-network-gateway-change"
  alarm_description   = "Alarms if cloudtrail logs show Network Gateway changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Network Gateway Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-route-table-change" {
  name           = "route-table-change"
  pattern        = "{ ($.eventName = CreateRoute) || ($.eventName = CreateRouteTable) || ($.eventName = ReplaceRoute) || ($.eventName = ReplaceRouteTableAssociation) || ($.eventName = DeleteRouteTable) || ($.eventName = DeleteRoute) || ($.eventName = DisassociateRouteTable) }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "Route Table Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-route-table-change" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-route-table-change"
  alarm_description   = "Alarms if cloudtrail logs show Route Table changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Route Table Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-vpc-change" {
  name           = "vpc-change"
  pattern        = "{ ($.eventName = CreateVpc) || ($.eventName = DeleteVpc) || ($.eventName = ModifyVpcAttribute) || ($.eventName = AcceptVpcPeeringConnection) || ($.eventName = CreateVpcPeeringConnection) || ($.eventName = DeleteVpcPeeringConnection) || ($.eventName = RejectVpcPeeringConnection) || ($.eventName = AttachClassicLinkVpc) || ($.eventName = DetachClassicLinkVpc) || ($.eventName = DisableVpcClassicLink) || ($.eventName = EnableVpcClassicLink) }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "VPC Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-vpc-change" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-vpc-change"
  alarm_description   = "Alarms if cloudtrail logs show VPC changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "VPC Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-no-mfa-console-login" {
  name           = "no-mfa-console-login"
  pattern        = "{ ($.eventName = \"ConsoleLogin\") && ($.additionalEventData.MFAUsed != \"Yes\") }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "No MFA Console Login"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-no-mfa-console-login" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-no-mfa-console-login"
  alarm_description   = "Alarms if cloudtrail logs show Console login with no MFA."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "No MFA Console Login"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-root-account-usage" {
  name           = "root-account-usage"
  pattern        = "{ $.userIdentity.type = \"Root\" && $.userIdentity.invokedBy NOT EXISTS && $.eventType != \"AwsServiceEvent\" }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "Root Account Usage"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-root-account-usage" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-root-account-usage"
  alarm_description   = "Alarms if cloudtrail logs show Root Account Usage."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Root Account Usage"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-iam-policy-changes" {
  name           = "iam-policy-changes"
  pattern        = "{($.eventName=DeleteGroupPolicy)||($.eventName=DeleteRolePolicy)||($.eventName=DeleteUserPolicy)||($.eventName=PutGroupPolicy)||($.eventName=PutRolePolicy)||($.eventName=PutUserPolicy)||($.eventName=CreatePolicy)||($.eventName=DeletePolicy)||($.eventName=CreatePolicyVersion)||($.eventName=DeletePolicyVersion)||($.eventName=AttachRolePolicy)||($.eventName=DetachRolePolicy)||($.eventName=AttachUserPolicy)||($.eventName=DetachUserPolicy)||($.eventName=AttachGroupPolicy)||($.eventName=DetachGroupPolicy)}"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "IAM Policy Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-iam-policy-changes" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-iam-policy-changes"
  alarm_description   = "Alarms if cloudtrail logs show IAM Policy Changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "IAM Policy Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-cloudtrail-changes" {
  name           = "cloudtrail-changes"
  pattern        = "{ ($.eventName = CreateTrail) || ($.eventName = UpdateTrail) || ($.eventName = DeleteTrail) || ($.eventName = StartLogging) || ($.eventName = StopLogging) }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "CloudTrail Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-cloudtrail-changes" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-cloudtrail-changes"
  alarm_description   = "Alarms if cloudtrail logs show CloudTrail Changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "CloudTrail Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-s3-bucket-policy-changes" {
  name           = "s3-bucket-policy-changes"
  pattern        = "{ ($.eventSource = s3.amazonaws.com) && (($.eventName = PutBucketAcl) || ($.eventName = PutBucketPolicy) || ($.eventName = PutBucketCors) || ($.eventName = PutBucketLifecycle) || ($.eventName = PutBucketReplication) || ($.eventName = DeleteBucketPolicy) || ($.eventName = DeleteBucketCors) || ($.eventName = DeleteBucketLifecycle) || ($.eventName = DeleteBucketReplication)) }"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "S3 Bucket Policy Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-s3-bucket-policy-changes" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-s3-bucket-policy-changes"
  alarm_description   = "Alarms if cloudtrail logs show S3 Bucket Policy Changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "S3 Bucket Policy Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-security-group-changes" {
  name           = "security-group-changes"
  pattern        = "{($.eventName=AuthorizeSecurityGroupIngress) || ($.eventName=AuthorizeSecurityGroupEgress) || ($.eventName=RevokeSecurityGroupIngress) || ($.eventName=RevokeSecurityGroupEgress) || ($.eventName=CreateSecurityGroup) || ($.eventName=DeleteSecurityGroup)}"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "Security Group Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-security-group-changes" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-security-group-changes"
  alarm_description   = "Alarms if cloudtrail logs show Security Group Changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "Security Group Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-nacl-changes" {
  name           = "nacl-changes"
  pattern        = "{($.eventName=CreateNetworkAcl) || ($.eventName=CreateNetworkAclEntry) || ($.eventName=DeleteNetworkAcl) || ($.eventName=DeleteNetworkAclEntry) || ($.eventName=ReplaceNetworkAclEntry) || ($.eventName=ReplaceNetworkAclAssociation)}"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "NACL Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-nacl-changes" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-nacl-changes"
  alarm_description   = "Alarms if cloudtrail logs show NACL Changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "NACL Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-aws-config-changes" {
  name           = "aws-config-changes"
  pattern        = "{($.eventSource=config.amazonaws.com) && (($.eventName=StopConfigurationRecorder) || ($.eventName=DeleteDeliveryChannel) || ($.eventName=PutDeliveryChannel) || ($.eventName=PutConfigurationRecorder))}"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "AWS Config Changes"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-aws-config-changes" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-aws-config-changes"
  alarm_description   = "Alarms if cloudtrail logs show AWS Config Changes."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "AWS Config Changes"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-console-auth-failure" {
  name           = "console-auth-failure"
  pattern        = "{($.eventName=ConsoleLogin) && ($.errorMessage=\"Failed authentication\")}"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "AWS Console Auth Failure"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-console-auth-failure" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-console-auth-failure"
  alarm_description   = "Alarms if cloudtrail logs show AWS Console Auth Failures."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "AWS Console Auth Failure"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}

resource "aws_cloudwatch_log_metric_filter" "cis-benchmark-kms-cmk-delete" {
  name           = "kms-cmk-delete"
  pattern        = "{($.eventSource=kms.amazonaws.com) && (($.eventName=DisableKey) || ($.eventName=ScheduleKeyDeletion))}"
  log_group_name = aws_cloudwatch_log_group.cloudtrail.name
  metric_transformation {
    name      = "KMS CMK Delete"
    namespace = "CISBenchmark"
    value     = "1"
  }
}

resource "aws_cloudwatch_metric_alarm" "cis-benchmark-kms-cmk-delete" {
  alarm_actions       = [aws_sns_topic.cloudtrail.arn]
  alarm_name          = "cis-benchmark-kms-cmk-delete"
  alarm_description   = "Alarms if cloudtrail logs show KMS CMK deletion."
  comparison_operator = "GreaterThanOrEqualToThreshold"
  evaluation_periods  = "1"
  metric_name         = "KMS CMK Delete"
  namespace           = "CISBenchmark"
  ok_actions          = [aws_sns_topic.cloudtrail.arn]
  period              = "300"
  statistic           = "Sum"
  threshold           = "1"
  treat_missing_data  = "notBreaching"
}
