resource "aws_s3_bucket" "cloudtrail" {
  acl    = "private"
  bucket = "cloudtrail-${var.region}-${data.aws_caller_identity.current.account_id}"
  lifecycle {
    ignore_changes = [
      "logging"
    ]
  }
  region = var.region
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        kms_master_key_id = aws_kms_key.cloudtrail.key_id
        sse_algorithm     = "aws:kms"
      }
    }
  }
  tags = {
    App = var.app
  }
}

data "template_file" "bucket-policy" {
  template = "${file("${path.module}/templates/cloudtrail-bucket-policy.json.tpl")}"
  vars = {
    aws_account_id  = data.aws_caller_identity.current.account_id
    organization-id = var.organization-id
    region          = var.region
  }
}

resource "aws_s3_bucket_policy" "cloudtrail" {
  bucket = aws_s3_bucket.cloudtrail.id
  policy = data.template_file.bucket-policy.rendered
}

resource "aws_s3_bucket_public_access_block" "cloudtrail" {
  block_public_acls       = true
  block_public_policy     = true
  bucket                  = aws_s3_bucket.cloudtrail.id
  ignore_public_acls      = true
  restrict_public_buckets = true
}

resource "aws_s3_bucket" "cloudtrail-access-logs" {
  acl    = "private"
  bucket = "cloudtrail-access-logs-${var.region}-${data.aws_caller_identity.current.account_id}"
  lifecycle_rule {
    prefix  = ""
    enabled = true
    expiration {
      days = 1830
    }
  }
  region = var.region
  server_side_encryption_configuration {
    rule {
      apply_server_side_encryption_by_default {
        sse_algorithm     = "AES256"
      }
    }
  }
  tags = {
    App = var.app
  }
}
