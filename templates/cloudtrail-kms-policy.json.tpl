{
  "Version":"2012-10-17",
  "Id":"Key policy created by CloudTrail",
  "Statement":[
    {
      "Sid":"Enable IAM User Permissions",
      "Effect":"Allow",
      "Principal":{
        "AWS":[
          "arn:aws:iam::${aws_account_id}:root"
        ]
      },
      "Action":"kms:*",
      "Resource":"*"
    },
    {
      "Sid":"Allow CloudTrail to encrypt logs",
      "Effect":"Allow",
      "Principal":{
        "Service":"cloudtrail.amazonaws.com"
      },
      "Action":"kms:GenerateDataKey*",
      "Resource":"*",
      "Condition":{
        "StringLike":{
          "kms:EncryptionContext:aws:cloudtrail:arn":"arn:aws:cloudtrail:*:${aws_account_id}:trail/*"
        }
      }
    },
    {
      "Sid":"Allow CloudTrail to describe key",
      "Effect":"Allow",
      "Principal":{
        "Service":"cloudtrail.amazonaws.com"
      },
      "Action":"kms:DescribeKey",
      "Resource":"*"
    },
    {
      "Sid":"Allow alias creation during setup",
      "Effect":"Allow",
      "Principal":{
        "AWS":"*"
      },
      "Action":"kms:CreateAlias",
      "Resource":"*",
      "Condition":{
        "StringEquals":{
          "kms:CallerAccount":"${aws_account_id}",
          "kms:ViaService":"ec2.us-east-1.amazonaws.com"
        }
      }
    },
    {
      "Effect": "Allow",
      "Principal": { "Service": "logs.us-east-1.amazonaws.com" },
      "Action": [
        "kms:Encrypt*",
        "kms:Decrypt*",
        "kms:ReEncrypt*",
        "kms:GenerateDataKey*",
        "kms:Describe*"
      ],
      "Resource": "*"
    }
  ]
}
