data "aws_iam_policy_document" "cloudtrail-assume-role" {
  statement {
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["cloudtrail.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "logs" {
  assume_role_policy = data.aws_iam_policy_document.cloudtrail-assume-role.json
  name               = "${var.app}-logs"
  tags = {
    App = var.app
  }
}

data "template_file" "logs" {
  template = file("${path.module}/templates/cloudtrail-logs-policy.json.tpl")
  vars = {
    aws_account_id  = data.aws_caller_identity.current.account_id
    organization-id = var.organization-id
    region          = var.region
  }
}

resource "aws_iam_role_policy" "logs" {
  name   = "logs-policy"
  policy = data.template_file.logs.rendered
  role   = aws_iam_role.logs.id
}

resource "aws_cloudwatch_log_group" "cloudtrail" {
  kms_key_id        = aws_kms_key.cloudtrail.arn
  name              = "/aws/cloudtrail"
  retention_in_days = "30"
  tags = {
    App = var.app
  }
}
