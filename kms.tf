data "template_file" "kms-policy" {
  template = "${file("${path.module}/templates/cloudtrail-kms-policy.json.tpl")}"
  vars = {
    aws_account_id = data.aws_caller_identity.current.account_id
  }
}

resource "aws_kms_key" "cloudtrail" {
  description         = "The encryption key for CloudTrail logs."
  enable_key_rotation = "true"
  policy              = data.template_file.kms-policy.rendered
  tags = {
    App = var.app
  }
}

resource "aws_kms_alias" "cloudtrail" {
  name          = "alias/cloudtrail"
  target_key_id = aws_kms_key.cloudtrail.key_id
}
