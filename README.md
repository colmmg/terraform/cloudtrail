# Introduction
This Terraform code creates a [CloudTrail](https://aws.amazon.com/cloudtrail/) trail for an [AWS Organization](https://aws.amazon.com/organizations/) together with an [S3](https://aws.amazon.com/s3/) bucket and [KMS CMK](https://aws.amazon.com/kms/).

The trail is configured to record events from all regions and S3 object level events.

Additionally, some [CloudWatch alarms](https://docs.aws.amazon.com/AmazonCloudWatch/latest/monitoring/AlarmThatSendsEmail.html) to meet [CIS Benchmarks 3.x](https://docs.aws.amazon.com/securityhub/latest/userguide/securityhub-cis-controls.html#securityhub-cis-controls-3.1) are created.

# Usage
Apply with:
```
terraform init
terraform apply
```

# Apply Issue
When applying or modifying the trail via Terraform you may get the following error:
```
Error: Error updating CloudTrail: InsufficientEncryptionPolicyException: Insufficient permissions to access S3 bucket cloudtrail-us-east-1-123456789012 or KMS key arn:aws:kms:us-east-1:123456789012:key/12345678-abcd-1234-abcd-123456789012.
        status code: 400, request id: 12345678-abcd-1234-abcd-123456789012

  on cloudtrail.tf line 1, in resource "aws_cloudtrail" "all-events-trail":
  1: resource "aws_cloudtrail" "all-events-trail" {
```

So you may need to create the trail manually and import it into Terraform. If you modify the trail and try to `terraform apply` you may hit the same issue.

# Server Access Logging
Terraform doesn't seem to support enabling S3 access logging yet. See [here](https://github.com/terraform-providers/terraform-provider-aws/issues/989). This stack does apply the cloudtrail-access-logs bucket. So after you apply this stack, enable logging for the cloudtrail bucket to the cloudtrail-access-logs manually by following the steps at [securityhub-cis-controls.html](https://docs.aws.amazon.com/securityhub/latest/userguide/securityhub-cis-controls.html#cis-2.6-remediation).

# MFA Delete
You should enable MFA delete on the bucket so that objects can't be removed without MFA.

First get the serial number of the MFA device from:
```
$ aws iam list-mfa-devices
```

Then, using the serial number and the 6 digit MFA run:
```
$ aws s3api put-bucket-versioning --bucket cloudtrail-us-east-1-123456789012 --versioning-configuration "Status=Enabled,MFADelete=Enabled" --mfa "MFA_SERIAL 123456"
```

# Athena Table Create SQL (Organizations)
The SQL to create the Athena table for CloudTrail at the organization level is:
```
CREATE EXTERNAL TABLE cloudtrail_logs (
eventversion STRING,
useridentity STRUCT<
               type:STRING,
               principalid:STRING,
               arn:STRING,
               accountid:STRING,
               invokedby:STRING,
               accesskeyid:STRING,
               userName:STRING,
sessioncontext:STRUCT<
attributes:STRUCT<
               mfaauthenticated:STRING,
               creationdate:STRING>,
sessionissuer:STRUCT<  
               type:STRING,
               principalId:STRING,
               arn:STRING, 
               accountId:STRING,
               userName:STRING>>>,
eventtime STRING,
eventsource STRING,
eventname STRING,
awsregion STRING,
sourceipaddress STRING,
useragent STRING,
errorcode STRING,
errormessage STRING,
requestparameters STRING,
responseelements STRING,
additionaleventdata STRING,
requestid STRING,
eventid STRING,
resources ARRAY<STRUCT<
               ARN:STRING,
               accountId:STRING,
               type:STRING>>,
eventtype STRING,
apiversion STRING,
readonly STRING,
recipientaccountid STRING,
serviceeventdetails STRING,
sharedeventid STRING,
vpcendpointid STRING
)
PARTITIONED BY (account string, region string, year string, month string)
ROW FORMAT SERDE 'com.amazon.emr.hive.serde.CloudTrailSerde'
STORED AS INPUTFORMAT 'com.amazon.emr.cloudtrail.CloudTrailInputFormat'
OUTPUTFORMAT 'org.apache.hadoop.hive.ql.io.HiveIgnoreKeyTextOutputFormat'
LOCATION 's3://cloudtrail-us-east-1-123456789012/AWSLogs/o-abcde12345/'
TBLPROPERTIES ('classification'='cloudtrail', 'has_encrypted_data'='true');
```

You can then create partitions with the following:
```
ALTER TABLE cloudtrail_logs 
ADD PARTITION (account='123456789012', region='us-east-1', year='2020', month='01') 
LOCATION 's3://cloudtrail-us-east-1-123456789012/AWSLogs/o-abcde12345/123456789012/CloudTrail/us-east-1/2020/01/';
```
