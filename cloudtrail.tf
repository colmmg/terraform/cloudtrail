resource "aws_cloudtrail" "all-events-trail" {
  cloud_watch_logs_group_arn    = aws_cloudwatch_log_group.cloudtrail.arn
  cloud_watch_logs_role_arn     = aws_iam_role.logs.arn
  depends_on                    = [aws_s3_bucket_policy.cloudtrail]
  enable_log_file_validation    = true
  event_selector {
    read_write_type           = "All"
    include_management_events = true
    data_resource {
      type   = "AWS::S3::Object"
      values = ["arn:aws:s3:::"]
    }
  }
  name                          = "all-events-trail"
  s3_bucket_name                = aws_s3_bucket.cloudtrail.id
  is_multi_region_trail         = true
  is_organization_trail         = true
  kms_key_id                    = aws_kms_key.cloudtrail.arn
  tags = {
    App = var.app
  }
}
