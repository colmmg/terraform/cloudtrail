variable "app" {
  default     = "cloudtrail"
  description = "The string to use for the App tag that will be applied to all created resources."
}

variable "organization-id" {
  description = "The unique identifier (ID) of your organization."
}

variable "region" {
  default     = "us-east-1"
  description = "The AWS region."
}
